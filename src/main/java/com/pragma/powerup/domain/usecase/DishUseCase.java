package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IDishServicePort;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.spi.IDishPersistencePort;

import java.util.List;
import java.util.Map;

public class DishUseCase implements IDishServicePort {

    private final IDishPersistencePort dishPersistencePort;

    public DishUseCase(IDishPersistencePort dishPersistencePort) {
        this.dishPersistencePort = dishPersistencePort;
    }

    @Override
    public void saveDish(DishModel dishModel) {
        dishPersistencePort.saveDish(dishModel);
    }

    @Override
    public void updateDish(Long id, Map<String, Object> fields) {
        dishPersistencePort.updateDish(id, fields);

    }

    @Override
    public void updateActiveField(Long id, Map<String, Object> fields) {
        dishPersistencePort.updateActiveField(id, fields);
    }

    @Override
    public List<DishModel> showMenu(Long restaurantId, Long categoryId, Integer elements) {
        return dishPersistencePort.showMenu(restaurantId, categoryId, elements);
    }
}
