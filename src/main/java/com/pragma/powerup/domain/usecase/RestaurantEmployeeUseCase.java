package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IRestEmployeeServicePort;
import com.pragma.powerup.domain.model.RestaurantEmployeeModel;
import com.pragma.powerup.domain.spi.IRestEmployeePersistencePort;

public class RestaurantEmployeeUseCase implements IRestEmployeeServicePort {

    private final IRestEmployeePersistencePort restEmployeePersistencePort;

    public RestaurantEmployeeUseCase(IRestEmployeePersistencePort restEmployeePersistencePort) {
        this.restEmployeePersistencePort = restEmployeePersistencePort;
    }

    @Override
    public void associateEmployee(RestaurantEmployeeModel restaurantEmployeeModel) {
        restEmployeePersistencePort.associateEmployee(restaurantEmployeeModel);
    }
}
