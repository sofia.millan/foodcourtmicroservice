package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IOrderServicePort;
import com.pragma.powerup.domain.model.OrderDishModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;
import com.pragma.powerup.domain.spi.IOrderPersistencePort;

import java.util.List;

public class OrderUseCase implements IOrderServicePort {

    private final IOrderPersistencePort orderPersistencePort;

    public OrderUseCase(IOrderPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;
    }

    @Override
    public void addOrder(OrderModel orderModel, Long clientId) {
        orderPersistencePort.addOrder(orderModel, clientId);
    }

    @Override
    public List<OrderModelResp> showOrders(Integer elements, String status, Long employeeId) {
       return orderPersistencePort.showOrders(elements, status, employeeId);
    }
}
