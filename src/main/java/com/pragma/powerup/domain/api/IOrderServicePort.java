package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;

import java.util.List;

public interface IOrderServicePort {

    void addOrder(OrderModel orderModel, Long clientId);

    List<OrderModelResp> showOrders(Integer elemnents, String status, Long idEmployee);
}
