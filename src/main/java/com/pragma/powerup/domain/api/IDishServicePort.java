package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.DishModel;
import java.util.List;
import java.util.Map;

public interface IDishServicePort {
    void saveDish(DishModel dishModel);

    void updateDish(Long id, Map<String, Object> fields);

    void updateActiveField(Long id, Map<String, Object> fields);

    List<DishModel> showMenu(Long restaurantId, Long categoryId, Integer elements);

}
