package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.dto.response.DishPageResponseDto;
import com.pragma.powerup.application.handler.IDishHandler;
import com.pragma.powerup.application.mapper.IDishRequestMapper;
import com.pragma.powerup.domain.api.IDishServicePort;
import com.pragma.powerup.domain.model.DishModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class DishHandler implements IDishHandler {
    private final IDishServicePort dishServicePort;
    private final IDishRequestMapper dishRequestMapper;
    @Override
    public void saveDish(DishRequestDto dishRequestDto) {
        DishModel dishModel = dishRequestMapper.toDish(dishRequestDto);
        dishServicePort.saveDish(dishModel);
    }


    @Override
    public void updateDish(Long id, Map<String, Object> fields) {
        dishServicePort.updateDish(id, fields);
    }

    @Override
    public void updateActiveField(Long id, Map<String, Object> fields) {
        dishServicePort.updateActiveField(id, fields);
    }

    @Override
    public List<DishPageResponseDto> showMenu(Long restaurantId, Long categoryId, Integer number) {
        List<DishModel> dishModelList = dishServicePort.showMenu(restaurantId,categoryId,number);

        System.out.println(dishModelList);
        List<DishPageResponseDto> dishPageResponseDtos = dishModelList.stream().map(dishModel -> dishRequestMapper.toPageDto(dishModel)).collect(Collectors.toList());

        System.out.println("Linea 48 handler");
        dishPageResponseDtos.forEach(dish-> System.out.println(dish.getName()));
        return dishPageResponseDtos;
    }


}
