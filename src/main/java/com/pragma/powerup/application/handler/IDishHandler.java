package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.dto.response.DishPageResponseDto;

import java.util.List;
import java.util.Map;

public interface IDishHandler {
    void saveDish(DishRequestDto dishRequestDto);


    void updateDish(Long id, Map<String, Object> fields);

    void updateActiveField(Long id, Map<String, Object> fields);

    List<DishPageResponseDto> showMenu(Long restaurantId, Long categoryId, Integer number);
}
