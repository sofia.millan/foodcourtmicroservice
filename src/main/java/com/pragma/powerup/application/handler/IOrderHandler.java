package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.dto.response.OrderPageResponseDto;
import java.util.List;
public interface IOrderHandler {
    void addOrder(OrderRequestDto orderRequestDto, Long clientId);
    List<OrderPageResponseDto> showOrders(Integer elemnents, String status, Long idEmployee);
}
