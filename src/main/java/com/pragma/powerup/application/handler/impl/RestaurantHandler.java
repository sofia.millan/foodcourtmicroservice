package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.RestaurantRequestDto;
import com.pragma.powerup.application.dto.response.RestaurantPageResponseDto;
import com.pragma.powerup.application.handler.IRestaurantHandler;
import com.pragma.powerup.application.mapper.IRestaurantRequestMapper;
import com.pragma.powerup.domain.api.IRestaurantServicePort;
import com.pragma.powerup.domain.model.RestaurantModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class RestaurantHandler implements IRestaurantHandler {
    private final IRestaurantServicePort servicePort;
    private final IRestaurantRequestMapper restaurantRequestMapper;


    @Override
    public void saveRestaurant(RestaurantRequestDto restaurantRequestDto) {
        RestaurantModel restaurantModel = restaurantRequestMapper.toRestaurant(restaurantRequestDto);
        servicePort.saveRestaurant(restaurantModel);
    }

    @Override
    public List<RestaurantPageResponseDto> findRestaurants(Integer number) {
        List<RestaurantModel> restaurantModelList = servicePort.findRestaurants(number);
        List<RestaurantPageResponseDto> restaurantPageResponseDtoList = restaurantModelList.stream().map(restaurantModel -> restaurantRequestMapper.toResponseDto(restaurantModel)).collect(Collectors.toList());
        return restaurantPageResponseDtoList;
    }

}
