package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.OrderDishRequestDto;
import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.dto.response.OrderPageResponseDto;
import com.pragma.powerup.domain.model.OrderDishModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IOrderRequestMapper {

    OrderModel toOrderModel(OrderRequestDto orderRequestDto);

    OrderRequestDto toOrderDto(OrderModel orderModel);

    OrderDishModel toOrderDishModel(OrderDishRequestDto orderDishRequestDto);

    OrderDishRequestDto toOrderDishDto(OrderDishModel orderDishModel);

    OrderPageResponseDto toResponseDto(OrderModelResp orderModelResp);
}
