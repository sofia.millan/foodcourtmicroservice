package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;
import com.pragma.powerup.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.infrastructure.exception.DataNotFoundException;
import com.pragma.powerup.infrastructure.exception.DataNotValidException;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import com.pragma.powerup.infrastructure.out.jpa.entity.*;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IOrderEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class OrderJpaAdapter implements IOrderPersistencePort {

    private final RestaurantRepository restaurantRepository;
    private final DishRepository dishRepository;
    private final OrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;
    private final OrderDishRepository orderDishRepository;
    private final IFeignValidations feignValidations;
    private final RestaurantEmployeeRepository chefRepository;
    @Override
    public OrderModel addOrder(OrderModel orderModel, Long clientId) {
        OrderEntity orderEntity = new OrderEntity();
        List<Long> dishIdsList = new ArrayList<>();
        List<Integer> amountList = new ArrayList<>();
        RestaurantEntity restaurant = restaurantRepository.findById(orderModel.getRestaurantId())
                .orElseThrow(DataNotFoundException::new);

        Integer countOrdersByClients = orderRepository.countOrders(clientId, orderModel.getRestaurantId());
        if(countOrdersByClients>0){
            throw new DataNotValidException();
        }
        orderModel.getOrderDishes().forEach(orderDish->{
            dishIdsList.add(orderDish.getDishId());
            amountList.add(orderDish.getAmount());
        });

        List<DishEntity> dishes = dishRepository.findAllById(dishIdsList);

        boolean isPartOfTheRestaurant = validatedish(orderModel.getRestaurantId(), dishes);
        if(!isPartOfTheRestaurant){
            throw new DataNotValidException();
        }
        orderEntity.setClientId(clientId);
        orderEntity.setDate(LocalDate.now());
        orderEntity.setRestaurant(restaurant);
        orderEntity.setStatus("PENDING");
        orderEntity.setOrderDishes(new HashSet<>());
        for(int i = 0; i< dishes.size();i++){
            OrderDishEntity orderDishEntity = new OrderDishEntity();
            orderDishEntity.setDish(dishes.get(i));
            orderDishEntity.setAmount(amountList.get(i));
            orderEntity.getOrderDishes().add(orderDishEntity);
        }

        OrderEntity savedOrder = orderRepository.save(orderEntity);
        for(OrderDishEntity orderDishEntity:orderEntity.getOrderDishes() ){
            orderDishEntity.setOrderEntity(savedOrder);
        }

        orderDishRepository.saveAll(orderEntity.getOrderDishes());

        return orderEntityMapper.toModel(orderEntity);
    }

    @Override
    public List<OrderModelResp> showOrders(Integer elements, String status, Long idEmployee) {

        RestaurantEmployeeEntity chef = chefRepository.findByUserId(idEmployee);

        Pageable pageable = PageRequest.of(0,elements);

        List<OrderEntity> orderEntityList = orderRepository.findAll(pageable).getContent();

        List<OrderEntity> orderEntityByStatus = orderEntityList
                .stream()
                .filter(order -> order.getStatus().equals(status))
                .filter(order -> order.getRestaurant().getId().equals(chef.getRestaurantId()))
                .collect(Collectors.toList());

     /*   List<OrderEntity> orderEntityByRestaurant = orderEntityByStatus
                .stream()
                .filter(order -> order.getRestaurant().getId().equals(chef.getRestaurantId()))
                .collect(Collectors.toList());*/
       // List<OrderEntity> orders = orderRepository.showOrders(chef.getRestaurantId());

       // System.out.println(orders);
        System.out.println("--- ");
        System.out.println(orderEntityByStatus);

        List<OrderModelResp> orderModelList = orderEntityByStatus.stream().map(orderEntityMapper::toModelResponse).collect(Collectors.toList());
        return orderModelList;
    }

    boolean validatedish(Long restaurantId, List<DishEntity> dishEntityList){
        for(DishEntity dish: dishEntityList){
            if(!dish.getRestaurant().getId().equals(restaurantId)){
                return false;
            }
        }
        return true;
    }
}
