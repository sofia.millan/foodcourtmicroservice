package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.RestaurantEmployeeModel;
import com.pragma.powerup.domain.spi.IRestEmployeePersistencePort;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRestEmployeeEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.RestaurantEmployeeRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RestEmployeeJpaAdapter implements IRestEmployeePersistencePort {

    private final RestaurantEmployeeRepository restaurantEmployeeRepository;
    private final IRestEmployeeEntityMapper restEmployeeEntityMapper;
    @Override
    public void associateEmployee(RestaurantEmployeeModel restaurantEmployeeModel) {
        restaurantEmployeeRepository.save(restEmployeeEntityMapper.toEntity(restaurantEmployeeModel));
    }
}
