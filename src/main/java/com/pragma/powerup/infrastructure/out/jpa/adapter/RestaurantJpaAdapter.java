package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.infrastructure.exception.DataAlreadyExistsException;
import com.pragma.powerup.infrastructure.exception.DataNotFoundException;
import com.pragma.powerup.infrastructure.exception.DataNotValidException;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final RestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;
    private final IFeignValidations feignValidations;
    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        if (!feignValidations.validateOwnerId(restaurantModel.getOwnerId())){
             throw new DataNotFoundException("Owner wih id"+ restaurantModel.getOwnerId()+" not found");
        }

        if(restaurantRepository.findByNit(restaurantModel.getNit()).isPresent()){
            throw new DataAlreadyExistsException();
        }
        RestaurantEntity restaurantEntity = restaurantRepository.save(restaurantEntityMapper.toRestaurantEntity(restaurantModel));
        return restaurantEntityMapper.toModel(restaurantEntity);
    }

    @Override
    public List<RestaurantModel> findRestaurants(Integer number) {
        if (number<0){
            throw new DataNotValidException();
        }
        Pageable pageable = PageRequest.of(0,number,Sort.by(Sort.Direction.ASC, "name"));

        List<RestaurantEntity> listRestaurantEntity = restaurantRepository.findAll(pageable).getContent();

        List<RestaurantModel> listRestaurantModel = listRestaurantEntity.stream().map(restaurantEntity -> restaurantEntityMapper.toModel(restaurantEntity)).collect(Collectors.toList());

        return listRestaurantModel;
    }


}
