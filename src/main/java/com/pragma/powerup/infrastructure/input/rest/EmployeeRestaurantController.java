package com.pragma.powerup.infrastructure.input.rest;


import com.pragma.powerup.application.dto.request.RestaurantEmployeeRequestDto;
import com.pragma.powerup.application.dto.request.RestaurantRequestDto;
import com.pragma.powerup.application.handler.IRestaurantEmployeeHandler;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class EmployeeRestaurantController {

    private final IRestaurantEmployeeHandler restaurantEmployeeHandler;

    @PostMapping("/employee")
    public ResponseEntity<Void> saveRestaurant(@RequestBody @Valid RestaurantEmployeeRequestDto restaurantEmployeeRequestDto) {
        restaurantEmployeeHandler.associateEmployee(restaurantEmployeeRequestDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
