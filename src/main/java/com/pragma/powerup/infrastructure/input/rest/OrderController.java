package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.dto.response.OrderPageResponseDto;
import com.pragma.powerup.application.handler.IOrderHandler;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name="Orders", description = "Order related operations")
@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class OrderController {
    private final IOrderHandler orderHandler;
    private final IFeignValidations feignValidations;

    @PostMapping("/order")
    public void addOrder(@RequestBody OrderRequestDto orderRequestDto,
                         @RequestHeader("Authorization") String token){
        Long clientId = feignValidations.findClientId(token);
        orderHandler.addOrder(orderRequestDto, clientId);
    }

    @PostMapping("orders/")
    public List<OrderPageResponseDto> pageOrders(@RequestParam Integer numberOfElements,
                                                 @RequestParam String status,
                                                 @RequestHeader("Authorization") String token){
        Long employeeId = feignValidations.findEmployeeId(token);

       return  orderHandler.showOrders(numberOfElements, status, employeeId);

    }
}
