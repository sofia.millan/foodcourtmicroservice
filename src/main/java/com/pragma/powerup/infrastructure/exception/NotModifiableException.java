package com.pragma.powerup.infrastructure.exception;

public class NotModifiableException extends RuntimeException{
    public NotModifiableException() {
    }
}
