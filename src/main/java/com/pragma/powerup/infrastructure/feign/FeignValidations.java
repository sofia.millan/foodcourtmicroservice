package com.pragma.powerup.infrastructure.feign;

import com.pragma.powerup.domain.spi.IDishPersistencePort;

import com.pragma.powerup.infrastructure.exception.ForbiddenException;
import com.pragma.powerup.infrastructure.exception.UnAuthorizedException;
import com.pragma.powerup.infrastructure.feign.constants.Role;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class FeignValidations implements IFeignValidations{

    private final UserServiceClient userServiceClient;
    private final IDishPersistencePort dishPersistencePort;

    public void validateAdministrator(String token){
        if(userServiceClient.roles(token).size()==0){
            throw new UnAuthorizedException();
        }
        if(!userServiceClient.roles(token).contains(Role.ADMIN.getMessage())){
            throw new ForbiddenException();
        }
    }
    public void validateOwner(String token){
        if(userServiceClient.roles(token).size()==0){
            throw new UnAuthorizedException();
        }
        if(!userServiceClient.roles(token).contains(Role.OWNER.getMessage())){
            throw new ForbiddenException();
        }
    }

    public void validateClient(String token){
        if(userServiceClient.roles(token).size()==0){
            throw new UnAuthorizedException();
        }
        if(!userServiceClient.roles(token).contains(Role.CLIENT.getMessage())){
            throw new ForbiddenException();
        }
    }
        public void validateEmployee(String token){
            if(userServiceClient.roles(token).size()==0){
                throw new UnAuthorizedException();
            }
            if(!userServiceClient.roles(token).contains(Role.EMPLOYEE.getMessage())){
                throw new ForbiddenException();
            }
    }

    public boolean validateOwnerId(Long id){
        return userServiceClient.findOwnerById(id);
    }

    @Override
    public boolean validateOwnerRestaurant(String token, Long idRestaurant) {
        Long idOwner = userServiceClient.finOwner(token);
        return dishPersistencePort.validateDishRestaurant(idOwner, idRestaurant);
    }


    public boolean validateOwnerRestaurantbyIdDish(String token, Long idDish) {
        Long idOwner = userServiceClient.finOwner(token);
        Long idRestaurant =  dishPersistencePort.findIdRestaurantInDish(idDish);
        return dishPersistencePort.validateDishRestaurant(idOwner, idRestaurant);
    }

    @Override
    public Long findClientId(String token) {
        return userServiceClient.findClientId(token);
    }

    @Override
    public Long findEmployeeId(String token) {
        return userServiceClient.findEmployeeId(token);
    }

}
