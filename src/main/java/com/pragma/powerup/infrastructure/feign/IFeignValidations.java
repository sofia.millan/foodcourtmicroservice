package com.pragma.powerup.infrastructure.feign;


public interface IFeignValidations {
     void validateAdministrator(String token);
     void validateOwner(String token);

     void validateClient(String token);

     void validateEmployee(String token);

     boolean validateOwnerId(Long id);

     boolean validateOwnerRestaurant(String token, Long id);

     boolean validateOwnerRestaurantbyIdDish(String token, Long idDish);

     Long findClientId(String token);

     Long findEmployeeId(String token);
}
