package com.pragma.powerup.infrastructure.configuration;

import com.pragma.powerup.infrastructure.feign.FeignValidations;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import com.pragma.powerup.infrastructure.feign.UserServiceClient;
import lombok.RequiredArgsConstructor;
import org.mapstruct.control.MappingControl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientConfig {

    @Bean
    System.Logger.Level feignLoggerLevel(){
        return System.Logger.Level.INFO;
    }
}