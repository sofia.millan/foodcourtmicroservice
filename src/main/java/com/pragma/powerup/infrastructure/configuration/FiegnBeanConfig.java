package com.pragma.powerup.infrastructure.configuration;

import com.pragma.powerup.domain.spi.IDishPersistencePort;
import com.pragma.powerup.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.infrastructure.feign.FeignValidations;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import com.pragma.powerup.infrastructure.feign.UserServiceClient;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FiegnBeanConfig {
    private final UserServiceClient userServiceClient;
    private final IDishPersistencePort dishPersistencePort;
    @Bean
    public IFeignValidations feignValidations(){
        return new FeignValidations(userServiceClient, dishPersistencePort);
    }
}
