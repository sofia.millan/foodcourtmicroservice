package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.domain.model.RestaurantEmployeeModel;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEmployeeEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-30T07:49:14-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11 (Oracle Corporation)"
)
@Component
public class IRestEmployeeEntityMapperImpl implements IRestEmployeeEntityMapper {

    @Override
    public RestaurantEmployeeModel toModel(RestaurantEmployeeEntity entity) {
        if ( entity == null ) {
            return null;
        }

        RestaurantEmployeeModel restaurantEmployeeModel = new RestaurantEmployeeModel();

        restaurantEmployeeModel.setRestaurantId( entity.getRestaurantId() );
        restaurantEmployeeModel.setUserId( entity.getUserId() );

        return restaurantEmployeeModel;
    }

    @Override
    public RestaurantEmployeeEntity toEntity(RestaurantEmployeeModel model) {
        if ( model == null ) {
            return null;
        }

        RestaurantEmployeeEntity restaurantEmployeeEntity = new RestaurantEmployeeEntity();

        restaurantEmployeeEntity.setUserId( model.getUserId() );
        restaurantEmployeeEntity.setRestaurantId( model.getRestaurantId() );

        return restaurantEmployeeEntity;
    }
}
