package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.domain.model.OrderDishModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;
import com.pragma.powerup.infrastructure.out.jpa.entity.OrderDishEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.OrderEntity;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-30T07:49:15-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11 (Oracle Corporation)"
)
@Component
public class IOrderEntityMapperImpl implements IOrderEntityMapper {

    @Override
    public OrderModel toModel(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        OrderModel orderModel = new OrderModel();

        orderModel.setOrderDishes( orderDishEntitySetToOrderDishModelList( orderEntity.getOrderDishes() ) );

        return orderModel;
    }

    @Override
    public OrderEntity toEntity(OrderModel orderModel) {
        if ( orderModel == null ) {
            return null;
        }

        OrderEntity orderEntity = new OrderEntity();

        orderEntity.setOrderDishes( orderDishModelListToOrderDishEntitySet( orderModel.getOrderDishes() ) );

        return orderEntity;
    }

    @Override
    public OrderDishModel toOrderDishModel(OrderDishEntity orderDishEntity) {
        if ( orderDishEntity == null ) {
            return null;
        }

        OrderDishModel orderDishModel = new OrderDishModel();

        orderDishModel.setAmount( orderDishEntity.getAmount() );

        return orderDishModel;
    }

    @Override
    public OrderDishEntity toOrderDishEntity(OrderDishModel orderDishModel) {
        if ( orderDishModel == null ) {
            return null;
        }

        OrderDishEntity orderDishEntity = new OrderDishEntity();

        orderDishEntity.setAmount( orderDishModel.getAmount() );

        return orderDishEntity;
    }

    @Override
    public OrderModelResp toModelResponse(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        OrderModelResp orderModelResp = new OrderModelResp();

        orderModelResp.setId( orderEntity.getId() );
        orderModelResp.setClientId( orderEntity.getClientId() );
        orderModelResp.setDate( orderEntity.getDate() );
        orderModelResp.setStatus( orderEntity.getStatus() );

        return orderModelResp;
    }

    protected List<OrderDishModel> orderDishEntitySetToOrderDishModelList(Set<OrderDishEntity> set) {
        if ( set == null ) {
            return null;
        }

        List<OrderDishModel> list = new ArrayList<OrderDishModel>( set.size() );
        for ( OrderDishEntity orderDishEntity : set ) {
            list.add( toOrderDishModel( orderDishEntity ) );
        }

        return list;
    }

    protected Set<OrderDishEntity> orderDishModelListToOrderDishEntitySet(List<OrderDishModel> list) {
        if ( list == null ) {
            return null;
        }

        Set<OrderDishEntity> set = new LinkedHashSet<OrderDishEntity>( Math.max( (int) ( list.size() / .75f ) + 1, 16 ) );
        for ( OrderDishModel orderDishModel : list ) {
            set.add( toOrderDishEntity( orderDishModel ) );
        }

        return set;
    }
}
