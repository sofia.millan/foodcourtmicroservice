package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.RestaurantEmployeeRequestDto;
import com.pragma.powerup.domain.model.RestaurantEmployeeModel;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-30T07:49:14-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11 (Oracle Corporation)"
)
@Component
public class IRestaurantEmployeeRequestMapperImpl implements IRestaurantEmployeeRequestMapper {

    @Override
    public RestaurantEmployeeModel toModel(RestaurantEmployeeRequestDto employeeRequestDto) {
        if ( employeeRequestDto == null ) {
            return null;
        }

        RestaurantEmployeeModel restaurantEmployeeModel = new RestaurantEmployeeModel();

        restaurantEmployeeModel.setRestaurantId( employeeRequestDto.getRestaurantId() );
        restaurantEmployeeModel.setUserId( employeeRequestDto.getUserId() );

        return restaurantEmployeeModel;
    }

    @Override
    public RestaurantEmployeeRequestDto toDto(RestaurantEmployeeModel restaurantEmployeeModel) {
        if ( restaurantEmployeeModel == null ) {
            return null;
        }

        RestaurantEmployeeRequestDto restaurantEmployeeRequestDto = new RestaurantEmployeeRequestDto();

        restaurantEmployeeRequestDto.setRestaurantId( restaurantEmployeeModel.getRestaurantId() );
        restaurantEmployeeRequestDto.setUserId( restaurantEmployeeModel.getUserId() );

        return restaurantEmployeeRequestDto;
    }
}
