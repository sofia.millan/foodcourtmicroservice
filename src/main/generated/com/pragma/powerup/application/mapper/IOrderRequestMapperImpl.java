package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.OrderDishRequestDto;
import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.dto.response.OrderPageResponseDto;
import com.pragma.powerup.domain.model.OrderDishModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.OrderModelResp;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-30T07:49:14-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11 (Oracle Corporation)"
)
@Component
public class IOrderRequestMapperImpl implements IOrderRequestMapper {

    @Override
    public OrderModel toOrderModel(OrderRequestDto orderRequestDto) {
        if ( orderRequestDto == null ) {
            return null;
        }

        OrderModel orderModel = new OrderModel();

        orderModel.setRestaurantId( orderRequestDto.getRestaurantId() );
        orderModel.setOrderDishes( orderDishRequestDtoListToOrderDishModelList( orderRequestDto.getOrderDishes() ) );

        return orderModel;
    }

    @Override
    public OrderRequestDto toOrderDto(OrderModel orderModel) {
        if ( orderModel == null ) {
            return null;
        }

        OrderRequestDto orderRequestDto = new OrderRequestDto();

        orderRequestDto.setRestaurantId( orderModel.getRestaurantId() );
        orderRequestDto.setOrderDishes( orderDishModelListToOrderDishRequestDtoList( orderModel.getOrderDishes() ) );

        return orderRequestDto;
    }

    @Override
    public OrderDishModel toOrderDishModel(OrderDishRequestDto orderDishRequestDto) {
        if ( orderDishRequestDto == null ) {
            return null;
        }

        OrderDishModel orderDishModel = new OrderDishModel();

        orderDishModel.setDishId( orderDishRequestDto.getDishId() );
        orderDishModel.setAmount( orderDishRequestDto.getAmount() );

        return orderDishModel;
    }

    @Override
    public OrderDishRequestDto toOrderDishDto(OrderDishModel orderDishModel) {
        if ( orderDishModel == null ) {
            return null;
        }

        OrderDishRequestDto orderDishRequestDto = new OrderDishRequestDto();

        orderDishRequestDto.setDishId( orderDishModel.getDishId() );
        orderDishRequestDto.setAmount( orderDishModel.getAmount() );

        return orderDishRequestDto;
    }

    @Override
    public OrderPageResponseDto toResponseDto(OrderModelResp orderModelResp) {
        if ( orderModelResp == null ) {
            return null;
        }

        OrderPageResponseDto orderPageResponseDto = new OrderPageResponseDto();

        orderPageResponseDto.setId( orderModelResp.getId() );
        orderPageResponseDto.setClientId( orderModelResp.getClientId() );
        orderPageResponseDto.setDate( orderModelResp.getDate() );
        orderPageResponseDto.setStatus( orderModelResp.getStatus() );

        return orderPageResponseDto;
    }

    protected List<OrderDishModel> orderDishRequestDtoListToOrderDishModelList(List<OrderDishRequestDto> list) {
        if ( list == null ) {
            return null;
        }

        List<OrderDishModel> list1 = new ArrayList<OrderDishModel>( list.size() );
        for ( OrderDishRequestDto orderDishRequestDto : list ) {
            list1.add( toOrderDishModel( orderDishRequestDto ) );
        }

        return list1;
    }

    protected List<OrderDishRequestDto> orderDishModelListToOrderDishRequestDtoList(List<OrderDishModel> list) {
        if ( list == null ) {
            return null;
        }

        List<OrderDishRequestDto> list1 = new ArrayList<OrderDishRequestDto>( list.size() );
        for ( OrderDishModel orderDishModel : list ) {
            list1.add( toOrderDishDto( orderDishModel ) );
        }

        return list1;
    }
}
