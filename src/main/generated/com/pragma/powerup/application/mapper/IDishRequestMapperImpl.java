package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.dto.response.DishPageResponseDto;
import com.pragma.powerup.domain.model.DishModel;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-30T07:49:14-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11 (Oracle Corporation)"
)
@Component
public class IDishRequestMapperImpl implements IDishRequestMapper {

    @Override
    public DishModel toDish(DishRequestDto dishRequestDto) {
        if ( dishRequestDto == null ) {
            return null;
        }

        DishModel dishModel = new DishModel();

        dishModel.setName( dishRequestDto.getName() );
        dishModel.setPrice( dishRequestDto.getPrice() );
        dishModel.setDescription( dishRequestDto.getDescription() );
        dishModel.setImageUrl( dishRequestDto.getImageUrl() );
        dishModel.setCategoryId( dishRequestDto.getCategoryId() );
        dishModel.setRestaurantId( dishRequestDto.getRestaurantId() );

        return dishModel;
    }

    @Override
    public DishPageResponseDto toPageDto(DishModel dishModel) {
        if ( dishModel == null ) {
            return null;
        }

        DishPageResponseDto dishPageResponseDto = new DishPageResponseDto();

        dishPageResponseDto.setName( dishModel.getName() );
        dishPageResponseDto.setPrice( dishModel.getPrice() );
        dishPageResponseDto.setDescription( dishModel.getDescription() );
        dishPageResponseDto.setImageUrl( dishModel.getImageUrl() );
        dishPageResponseDto.setActive( dishModel.isActive() );

        return dishPageResponseDto;
    }
}
