package com.pragma.powerup;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.infrastructure.exception.DataNotFoundException;
import com.pragma.powerup.infrastructure.exception.NotModifiableException;
import com.pragma.powerup.infrastructure.out.jpa.adapter.DishJpaAdapter;
import com.pragma.powerup.infrastructure.out.jpa.entity.CategoryEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.CategoryRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.DishRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.RestaurantRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class DishAdapterTest {
    @MockBean
    private RestaurantRepository restaurantRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private DishRepository dishRepository;
    @MockBean
    private IDishEntityMapper dishEntityMapper;
    @InjectMocks
    @Autowired
    private DishJpaAdapter dishJpaAdapter;



    @Test
    void Should_ThrowException_When_RestaurantIdNotExist(){
        DishModel dishDto = new DishModel(1L,"",30000,"This is the description",
                "https:image.png",true,1L,1L);

        when(restaurantRepository.findById(dishDto.getRestaurantId())).thenReturn(Optional.empty());
        when(categoryRepository.findById(dishDto.getCategoryId())).thenReturn(Optional.of(new CategoryEntity()));

        assertThrows(DataNotFoundException.class, ()-> dishJpaAdapter.saveDish(dishDto));
    }

    @Test
    void Should_ThrowException_When_CategoryIdNotExist(){
        DishModel dishDto = new DishModel(1L,"",30000,"This is the description",
                "https:image.png",true,1L,1L);

        when(restaurantRepository.findById(dishDto.getRestaurantId())).thenReturn(Optional.of(new RestaurantEntity()));
        when(categoryRepository.findById(dishDto.getCategoryId())).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, ()-> dishJpaAdapter.saveDish(dishDto));
    }

    @Test
    void Should_UpdateActiveFields(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("active",false);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));
        dishJpaAdapter.updateActiveField(idSearch,fields);
        verify(dishRepository).save(any());
    }

   @Test
    void Should_UpdatePrice(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("price",20000);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));
        dishJpaAdapter.updateDish(idSearch,fields);
        verify(dishRepository).save(any());
    }

    @Test
    void Should_UpdateDescription(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("description","This is the new description");

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));
        dishJpaAdapter.updateDish(idSearch,fields);

        verify(dishRepository).save(any());
    }

    @Test
    void Should_ThrowException_When_DishIdNotFound(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("price",20000);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, ()-> dishJpaAdapter.updateDish(idSearch, fields));
    }
    @Test
    void Should_ThrowException_When_UpdateNameByUpdateDish(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("name","Pasta");

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateDish(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateCategoryIdByUpdateDish(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("categoryId",2L);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateDish(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateRestaurantIdByUpdateDish(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("restaurantId",2L);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateDish(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateActiveByUpdateDish(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("active",false);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateDish(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdatePriceByUpdateActiveField(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("price",20000);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateActiveField(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateDescriptionByUpdateActiveField(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("description","This is a description");

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateActiveField(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateCategoryIdByUpdateActiveField(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("categoryId",2L);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateActiveField(idSearch, fields));
    }

    @Test
    void Should_ThrowException_When_UpdateNameByUpdateActiveField(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("name","Pasta");

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateActiveField(idSearch, fields));
    }
    @Test
    void Should_ThrowException_When_UpdateRestaurantIdByUpdateActiveField(){
        Long idSearch = 1L;
        Map<String, Object> fields = new HashMap<>();
        fields.put("restaurantId",2L);

        when(dishRepository.findById(idSearch)).thenReturn(Optional.of(new DishEntity()));

        assertThrows(NotModifiableException.class, ()-> dishJpaAdapter.updateActiveField(idSearch, fields));
    }







}
