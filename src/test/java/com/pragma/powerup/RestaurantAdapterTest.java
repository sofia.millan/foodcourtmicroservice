package com.pragma.powerup;

import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.infrastructure.exception.DataAlreadyExistsException;
import com.pragma.powerup.infrastructure.exception.DataNotFoundException;
import com.pragma.powerup.infrastructure.exception.DataNotValidException;
import com.pragma.powerup.infrastructure.feign.IFeignValidations;
import com.pragma.powerup.infrastructure.feign.UserServiceClient;
import com.pragma.powerup.infrastructure.out.jpa.adapter.RestaurantJpaAdapter;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.RestaurantRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@SpringBootTest

public class RestaurantAdapterTest {
    @MockBean
    private  RestaurantRepository restaurantRepository;
    @MockBean
    private  IRestaurantEntityMapper restaurantEntityMapper;
    @MockBean
    private IFeignValidations feignValidations;

    @InjectMocks
    @Autowired
    private RestaurantJpaAdapter restaurantJpaAdapter;

    @Test
    void Should_ThrowException_When_OwnerIdDoesNoExist(){
        Long idOwner = 1L;
        when(feignValidations.validateOwnerId(idOwner)).thenReturn(false);


        assertThrows(DataNotFoundException.class, ()-> restaurantJpaAdapter.saveRestaurant(new RestaurantModel()));
    }

    @Test
    void Should_ThrowException_When_NitAlreadyExists(){
        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setNit("15685");
        restaurantModel.setOwnerId(1L);
        restaurantModel.setAddress("Cll 35");
        restaurantModel.setName("Subway");
        restaurantModel.setLogoUrl("https:image.png");
        restaurantModel.setPhoneNumber("56985685");

        String nit = "15685";
        when(feignValidations.validateOwnerId(restaurantModel.getOwnerId())).thenReturn(true);
        when(restaurantRepository.findByNit(nit)).thenReturn(Optional.of(new RestaurantEntity()));

        assertThrows(DataAlreadyExistsException.class, ()-> restaurantJpaAdapter.saveRestaurant(restaurantModel));
    }

    @Test
    void mustSaveRestaurant(){
        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setNit("15685");
        restaurantModel.setOwnerId(1L);
        restaurantModel.setAddress("Cll 35");
        restaurantModel.setName("Subway");
        restaurantModel.setLogoUrl("https:image.png");
        restaurantModel.setPhoneNumber("56985685");
        String nit = "15685";

        when(feignValidations.validateOwnerId(restaurantModel.getOwnerId())).thenReturn(true);
        when(restaurantRepository.findByNit(nit)).thenReturn(Optional.empty());
        restaurantJpaAdapter.saveRestaurant(restaurantModel);

        verify(restaurantRepository).save(restaurantEntityMapper.toRestaurantEntity(restaurantModel));
    }

  /*  @Test
    void mustFindRestaurants(){
        int numberOfRestaurants = 3;
       *//* List<RestaurantEntity> restaurantEntityList = new ArrayList<>();
        restaurantEntityList.add(new RestaurantEntity());
        restaurantEntityList.add(new RestaurantEntity());*//*

        Pageable pageable = PageRequest.of(0,numberOfRestaurants, Sort.by(Sort.Direction.ASC, "name"));

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setNit("15685");
        restaurantModel.setOwnerId(1L);
        restaurantModel.setAddress("Cll 35");
        restaurantModel.setName("Subway");
        restaurantModel.setLogoUrl("https:image.png");
        restaurantModel.setPhoneNumber("56985685");

        when(feignValidations.validateOwnerId(restaurantModel.getOwnerId())).thenReturn(true);
        restaurantJpaAdapter.saveRestaurant(restaurantModel);

        List<RestaurantEntity> restaurantEntities = restaurantRepository.findAll(pageable).getContent();

        assertTrue(restaurantEntities.size()<=3);
    }*/

    @Test
    void Should_ThrowException_When_NumberOfElementsIsNegative(){
        int numberOfElements = -2;

        assertThrows(DataNotValidException.class, ()-> restaurantJpaAdapter.findRestaurants(numberOfElements));
    }
}
